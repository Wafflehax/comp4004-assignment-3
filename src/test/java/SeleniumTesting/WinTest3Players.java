package SeleniumTesting;

import java.io.File;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class WinTest3Players {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();

    @Before
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/cDriver/chromedriver.exe");
        driver = new ChromeDriver();
        baseUrl = "https://www.katalon.com/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    @Test
    public void testWinTest3Players() throws Exception {
        File file = new File("src/test/resources/selScen3.txt");
        String rigPath = file.getAbsolutePath();

        driver.get("http://localhost:8080/");
        driver.findElement(By.id("name")).click();
        driver.findElement(By.id("name")).clear();
        driver.findElement(By.id("name")).sendKeys("3PlayerWinTest");
        driver.findElement(By.id("connect")).click();
        driver.findElement(By.id("adminSelect")).click();
        new Select(driver.findElement(By.id("adminSelect"))).selectByVisibleText("2");
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='How many AIPlayers?'])[1]/following::option[2]")).click();
        driver.findElement(By.id("rigfile")).click();
        driver.findElement(By.id("rigfile")).clear();
        driver.findElement(By.id("rigfile")).sendKeys(rigPath.replace("/","\\"));
        driver.findElement(By.id("upload")).click();
        driver.findElement(By.id("swapCardsButton")).click();
        assertEquals("HA", driver.findElement(By.id("c1")).getText());
        assertEquals("DA", driver.findElement(By.id("c2")).getText());
        assertEquals("SA", driver.findElement(By.id("c3")).getText());
        assertEquals("CA", driver.findElement(By.id("c4")).getText());
        String source = driver.getPageSource();
        assertTrue(source.contains("3PlayerWinTest has swapped 0 cards."));
        assertTrue(source.contains("AI0 has swapped 0 cards."));
        assertTrue(source.contains("AI1 has swapped 0 cards."));
        assertTrue(source.contains("3PlayerWinTest wins!"));
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}

