package CucumberTesting;
import Poker.*;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;

public class AIPOtherSwaps {
AIP testAIP = new AIP();

    @Given("^AIP has three cards in sequence$")
    public void aipHasThreeCardsInSequence() {
        testAIP.hand = new Hand();
        testAIP.hand.addCard(new Card("C7"));
        testAIP.hand.addCard(new Card("C8"));
        testAIP.hand.addCard(new Card("D9"));
        testAIP.hand.addCard(new Card("HK"));
        testAIP.hand.addCard(new Card("S2"));

        System.out.println("TestAIP's Hand");
        testAIP.hand.printHand();
    }

    @Given("^AIP has three cards to a flush$")
    public void aipHasThreeCardsToAFlush() {
        testAIP.hand = new Hand();
        testAIP.hand.addCard(new Card("C7"));
        testAIP.hand.addCard(new Card("C8"));
        testAIP.hand.addCard(new Card("CA"));
        testAIP.hand.addCard(new Card("HK"));
        testAIP.hand.addCard(new Card("S2"));

        System.out.println("TestAIP's Hand");
        testAIP.hand.printHand();
    }

    @Given("^AIP has one pair$")
    public void aipHasOnePair() {
        testAIP.hand = new Hand();
        testAIP.hand.addCard(new Card("C7"));
        testAIP.hand.addCard(new Card("C5"));
        testAIP.hand.addCard(new Card("D7"));
        testAIP.hand.addCard(new Card("HK"));
        testAIP.hand.addCard(new Card("S2"));

        System.out.println("TestAIP's Hand");
        testAIP.hand.printHand();
    }

    @Given("^AIP has highcard$")
    public void aipHasHighcard() {
        testAIP.hand = new Hand();
        testAIP.hand.addCard(new Card("S4"));
        testAIP.hand.addCard(new Card("C7"));
        testAIP.hand.addCard(new Card("D9"));
        testAIP.hand.addCard(new Card("HK"));
        testAIP.hand.addCard(new Card("S2"));

        System.out.println("TestAIP's Hand");
        testAIP.hand.printHand();
    }

    @Then("^AIP swaps the out-of-sequence cards$")
    public void aipSwapsTheOutOfSequenceCards() {
        testAIP.runAI();
        testAIP.printCardsToSwap();
    }

    @Then("^AIP swaps the unsuited cards$")
    public void aipSwapsTheUnsuitedCards() {
        testAIP.runAI();
        testAIP.printCardsToSwap();
    }

    @Then("^AIP swaps the unpaired cards$")
    public void aipSwapsTheUnpairedCards() throws Throwable {
        testAIP.runAI();
        testAIP.printCardsToSwap();
    }

    @Then("^AIP swaps the lowest rank cards$")
    public void aipSwapsTheLowestRankCards() throws Throwable {
        testAIP.runAI();
        testAIP.printCardsToSwap();
    }
}
