package CucumberTesting;
import Poker.*;
import cucumber.annotation.en.And;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.runtime.PendingException;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class WinValidation {
    Player testHTB = new Player();
    AIP testAIP = new AIP();
    Game testGame = new Game();

    public void testGameAssign(){
        testGame.htb = testHTB;
        testGame.aip = testAIP;
    }

    @Given("^HTB has \"([^\"]*)\" and AIP has \"([^\"]*)\"$")
    public void htbHasAndAIPHas(String HTBhand, String lesserhand) {
        testGameAssign();
        playerDeal(HTBhand,testHTB);
        playerDeal(lesserhand,testAIP);
    }

    @Given("^AIP has \"([^\"]*)\" and HTB has \"([^\"]*)\"$")
    public void aipHasAndHTBHas(String AIPhand, String lesserhand) {
        testGameAssign();
        playerDeal(AIPhand,testAIP);
        playerDeal(lesserhand,testHTB);
    }

    @Given("^AIP is one away from \"([^\"]*)\"$")
    public void aipIsOneAwayFrom(String bigHand) {
        testGameAssign();
        testGame.gameDeck = new Deck("DJ");
        if(bigHand.compareTo("royalflush")==0){
            testAIP.hand.addCard(new Card("DA"));
            testAIP.hand.addCard(new Card("DK"));
            testAIP.hand.addCard(new Card("DQ"));
            testAIP.hand.addCard(new Card("D10"));
            testAIP.hand.addCard(new Card("H5")); //card that AIP will swap

            testAIP.hand.refreshStatus();
            assertEquals(testAIP.hand.getStatus(),"highcard");
        }

        else if(bigHand.compareTo("straightflush")==0)
        {
            testAIP.hand.addCard(new Card("D9"));
            testAIP.hand.addCard(new Card("D8"));
            testAIP.hand.addCard(new Card("DQ"));
            testAIP.hand.addCard(new Card("D10"));
            testAIP.hand.addCard(new Card("H5")); //card that AIP will swap

            testAIP.hand.refreshStatus();
            assertEquals(testAIP.hand.getStatus(),"highcard");
        }

        else if(bigHand.compareTo("flush")==0)
        {
            testAIP.hand.addCard(new Card("DA"));
            testAIP.hand.addCard(new Card("D8"));
            testAIP.hand.addCard(new Card("DK"));
            testAIP.hand.addCard(new Card("D2"));
            testAIP.hand.addCard(new Card("H5")); //card that AIP will swap

            testAIP.hand.refreshStatus();
            assertEquals(testAIP.hand.getStatus(),"highcard");
        }

        else if(bigHand.compareTo("straight")==0)
        {
            testAIP.hand.addCard(new Card("D9"));
            testAIP.hand.addCard(new Card("S8"));
            testAIP.hand.addCard(new Card("CQ"));
            testAIP.hand.addCard(new Card("H10"));
            testAIP.hand.addCard(new Card("H5")); //card that AIP will swap

            testAIP.hand.refreshStatus();
            assertEquals(testAIP.hand.getStatus(),"highcard");
        }
        else if(bigHand.compareTo("fullhouse_3ofakind")==0)
        {
            testAIP.hand.addCard(new Card("D8"));
            testAIP.hand.addCard(new Card("S8"));
            testAIP.hand.addCard(new Card("C8"));
            testAIP.hand.addCard(new Card("SJ"));
            testAIP.hand.addCard(new Card("H5")); //card that AIP will swap

            testAIP.hand.refreshStatus();
            assertEquals(testAIP.hand.getStatus(),"3ofakind");
        }

        else if(bigHand.compareTo("fullhouse_2pair")==0)
        {
            testAIP.hand.addCard(new Card("D8"));
            testAIP.hand.addCard(new Card("S8"));
            testAIP.hand.addCard(new Card("CJ"));
            testAIP.hand.addCard(new Card("HJ"));
            testAIP.hand.addCard(new Card("H5")); //card that AIP will swap

            testAIP.hand.refreshStatus();
            assertEquals(testAIP.hand.getStatus(),"2pair");
        }
    }

    @And("^HTB has \"([^\"]*)\" hand$")
    public void htbHasHand(String hand) {
        if(hand.compareTo("winning")==0)
        {
            playerDeal("royalflush",testHTB);
        }
        else
        {
            playerDeal("highcard",testHTB);
        }
    }

    @Then("^HTB wins$")
    public void htbWins() {
        Player winner = testGame.resolveWinner();
        System.out.println();
        System.out.println("HTB-hand: "+testHTB.hand.toString());
        System.out.println("vs.");
        System.out.println("AIP-hand: "+testAIP.hand.toString());
        System.out.println("assertEquals(winner,testHTB);");
        System.out.println();
        assertEquals(winner,testHTB);
    }

    @Then("^AIP wins$")
    public void aipWins()  {
        testGame.playGame(false,false);
        System.out.println("assertEquals(testGame.winner,testAIP);");
        System.out.println("assertEquals(testAIP.getCardsToSwap().size(),0);");
        System.out.println();
        assertEquals(testGame.winner,testAIP);
        assertEquals(testAIP.getCardsToSwap().size(),0);
    }

    @Then("^AIP swaps and \"([^\"]*)\"$")
    public void aipSwapsAnd(String outcome) {

        if(outcome.compareTo("wins")==0)
        {
            System.out.println();
            System.out.println("-----> ::Expecting AIP win:: <-----");
            testGame.playGame(false,false);
            System.out.println("assertEquals(testGame.winner,testAIP);");
            System.out.println("assertEquals(testAIP.getCardsToSwap().get(0).toString(),\"H5\");");

            assertEquals(testGame.winner,testAIP);
            assertEquals(testAIP.getCardsToSwap().get(0).toString(),"H5");
        }
        else
        {   System.out.println();
            System.out.println("-----> ::Expecting HTB win:: <-----");
            testGame.playGame(false,false);
            System.out.println("assertEquals(testGame.winner,testHTB);");
            System.out.println("assertEquals(testAIP.getCardsToSwap().get(0).toString(),\"H5\");");

            assertEquals(testGame.winner,testHTB);
            assertEquals(testAIP.getCardsToSwap().get(0).toString(),"H5");
        }
        System.out.println();
    }



//HELPER FUNCTIONS
    public void dealRoyalFlush(Player toTest) {
        //System.out.println("\n\t::Dealing a royal flush::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("HA"));
        toTest.hand.addCard(new Card("HK"));
        toTest.hand.addCard(new Card("HQ"));
        toTest.hand.addCard(new Card("HJ"));
        toTest.hand.addCard(new Card("H10"));
    }

    public void dealStraightFlush(Player toTest) {
        //System.out.println("\n\t::Dealing a straight flush::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("S3"));
        toTest.hand.addCard(new Card("S4"));
        toTest.hand.addCard(new Card("S5"));
        toTest.hand.addCard(new Card("S6"));
        toTest.hand.addCard(new Card("S7"));
    }


    public void deal4OfAKind(Player toTest) {
        //System.out.println("\n\t::Dealing a Four of A Kind::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("S3"));
        toTest.hand.addCard(new Card("H3"));
        toTest.hand.addCard(new Card("C3"));
        toTest.hand.addCard(new Card("D3"));
        toTest.hand.addCard(new Card("S7"));
    }

    public void dealFullHouse(Player toTest) {
        //System.out.println("\n\t::Dealing a Full House::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("S3"));
        toTest.hand.addCard(new Card("H3"));
        toTest.hand.addCard(new Card("C3"));
        toTest.hand.addCard(new Card("D7"));
        toTest.hand.addCard(new Card("S7"));
    }

    public void dealFlush(Player toTest) {
        //System.out.println("\n\t::Dealing a flush::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("S3"));
        toTest.hand.addCard(new Card("S5"));
        toTest.hand.addCard(new Card("S8"));
        toTest.hand.addCard(new Card("S9"));
        toTest.hand.addCard(new Card("SK"));
    }

    public void dealStraight(Player toTest) {
        //System.out.println("\n\t::Dealing a straight::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("C5"));
        toTest.hand.addCard(new Card("C6"));
        toTest.hand.addCard(new Card("H7"));
        toTest.hand.addCard(new Card("S8"));
        toTest.hand.addCard(new Card("D9"));
    }


    public void deal3OfAKind(Player toTest) {
        //System.out.println("\n\t::Dealing a three of a kind::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("SK"));
        toTest.hand.addCard(new Card("HK"));
        toTest.hand.addCard(new Card("DK"));
        toTest.hand.addCard(new Card("S5"));
        toTest.hand.addCard(new Card("H2"));
    }

    public void deal2Pair(Player toTest) {
        //System.out.println("\n\t::Dealing a two pair::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("D10"));
        toTest.hand.addCard(new Card("S10"));
        toTest.hand.addCard(new Card("H5"));
        toTest.hand.addCard(new Card("C5"));
        toTest.hand.addCard(new Card("D9"));
    }

    public void deal1Pair(Player toTest) {
        //System.out.println("\n\t::Dealing a one pair::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("CQ"));
        toTest.hand.addCard(new Card("HQ"));
        toTest.hand.addCard(new Card("S8"));
        toTest.hand.addCard(new Card("C3"));
        toTest.hand.addCard(new Card("DA"));
    }

    public void dealHighCard(Player toTest) {
        //System.out.println("\n\t::Dealing a high card::");

        toTest.hand = new Hand();
        toTest.hand.addCard(new Card("CQ"));
        toTest.hand.addCard(new Card("H10"));
        toTest.hand.addCard(new Card("S8"));
        toTest.hand.addCard(new Card("C3"));
        toTest.hand.addCard(new Card("DA"));
    }

    public void playerDeal(String toDeal, Player toTest){
        if(toDeal.compareTo("royalflush")==0) dealRoyalFlush(toTest);
        else if(toDeal.compareTo("straightflush")==0) dealStraightFlush(toTest);
        else if(toDeal.compareTo("4ofakind")==0) deal4OfAKind(toTest);
        else if(toDeal.compareTo("fullhouse")==0) dealFullHouse(toTest);
        else if(toDeal.compareTo("flush")==0) dealFlush(toTest);
        else if(toDeal.compareTo("straight")==0) dealStraight(toTest);
        else if(toDeal.compareTo("3ofakind")==0) deal3OfAKind(toTest);
        else if(toDeal.compareTo("2pair")==0) deal2Pair(toTest);
        else if (toDeal.compareTo("1pair")==0) deal1Pair(toTest);
        else dealHighCard(toTest);
    }


    @Given("^AIP has \"([^\"]*)\"$")
    public void aipHas(String AIPhand) {
        testGameAssign();
        testAIP.hand = new Hand();

        String [] cards = AIPhand.split(" ");

        for(int i = 0; i<cards.length;i++)
            testAIP.hand.addCard(new Card(cards[i]));


        System.out.println("testAIP's hand");
        System.out.println(testAIP.hand.toString());
        System.out.println();
    }

    @And("^HTB has \"([^\"]*)\"$")
    public void htbHas(String HTBhand) {
        testGameAssign();
        testHTB.hand = new Hand();

        String [] cards = HTBhand.split(" ");

        for(int i = 0; i<cards.length;i++)
            testHTB.hand.addCard(new Card(cards[i]));


        System.out.println("testHTB's hand");
        System.out.println(testHTB.hand.toString());
        System.out.println();
    }

    @Then("^\"([^\"]*)\" will win$")
    public void willWin(String player) {
        Player winner = testGame.resolveWinner();

        if(player.compareTo("AIP")==0) assertEquals(testGame.resolveWinner(),testAIP);
        else assertEquals(winner,testHTB);

        System.out.println("Winner of tiebreaker is...");
        System.out.println(winner.getName()+" with "+winner.hand.getStatus());
        System.out.println();
        System.out.println();
    }
}
