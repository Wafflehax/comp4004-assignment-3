package Junit;

import Poker.Deck;
import Poker.Hand;
import org.junit.Test;

import static org.junit.Assert.*;

public class DeckTest {

    @Test
    public void test(){
        Deck d = new Deck();
        Hand h = new Hand();
        deckInitTest(d);
        takeCardTest(d, h);
    }
public void deckInitTest(Deck d){
        assertEquals(52,d.getCards().size());
}

public void takeCardTest(Deck d, Hand h){
        int s1 = d.getCards().size();
        int s2;
        h.addCard(d.takeTopCard());
        s2 = d.getCards().size();
        assertEquals(1,s1-s2);
        assertEquals(false,d.getCards().contains(h.getCards()));

}



}