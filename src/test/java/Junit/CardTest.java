package Junit;

import Poker.Card;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {

    @Test
    public void test(){
        cardCheck();



    }
public void cardCheck(){
    Card check = new Card("HA");
    assertEquals(check.getSuit(),"H");
    assertEquals(check.getRank(),14);
    assertEquals(3, check.getSuitScore());

    check = new Card("D2");
    assertEquals(check.getSuit(),"D");
    assertEquals(check.getRank(),2);
    assertEquals(2, check.getSuitScore());

    //Compare card of same suit, H2 vs. D2, should return H2
    Card check2 = new Card("H2");
    assertEquals(check2,check2.higherCard(check));

}
}