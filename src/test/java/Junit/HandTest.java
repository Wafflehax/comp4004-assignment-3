package Junit;

import Poker.Card;
import Poker.Hand;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class HandTest {

    @Test
    public void test(){
        statusCheck();

    }

    public void statusCheck(){
    Hand h = new Hand();

    //Checking Royal Flush
    addCards(h, new String[]{"HA", "HQ","HJ","HK","H10"});
    Collections.shuffle(h.getCards());
    h.refreshStatus();
    assertEquals("royalflush",h.getStatus());
    h.getCards().clear();

    //Checking Straight Flush
        addCards(h, new String[]{"H9", "HK","HQ","HJ","H10"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("straightflush",h.getStatus());
        h.getCards().clear();

    //Checking 4 of a Kind
        addCards(h, new String[]{"SA", "HA","DA","CA","H3"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("4ofakind",h.getStatus());
        h.getCards().clear();

        //Checking 4 of a Kind
        addCards(h, new String[]{"H3", "HA","DA","CA","SA"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("4ofakind",h.getStatus());
        h.getCards().clear();

        //Checking 4 of a Kind
        addCards(h, new String[]{"SA", "HA","H3","CA","DA"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("4ofakind",h.getStatus());
        h.getCards().clear();

    //Checking Full-House
        addCards(h, new String[]{"HA", "CA","DA","HJ","CJ"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("fullhouse",h.getStatus());
        h.getCards().clear();

    //Checking Flush
        addCards(h, new String[]{"H3", "H6","H10","H8","H2"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("flush",h.getStatus());
        h.getCards().clear();

    //Checking Straight
        addCards(h, new String[]{"DA", "C2","S3","H4","H5"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("straight",h.getStatus());
        h.getCards().clear();

        addCards(h, new String[]{"DA", "C10","SQ","HJ","DK"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("straight",h.getStatus());
        h.getCards().clear();

    //Checking 3 of a Kind
        addCards(h, new String[]{"H2", "C2","D2","C6","C10"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("3ofakind",h.getStatus());
        h.getCards().clear();

    //Checking 2 Pair
        addCards(h, new String[]{"HA", "CA","H3","C3","D2"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("2pair",h.getStatus());
        h.getCards().clear();

    //Checking 1 Pair
        addCards(h, new String[]{"S4", "S4","H3","HA","D10"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("1pair",h.getStatus());
        h.getCards().clear();

    //Checking Highcard
        addCards(h, new String[]{"HA", "HK","HQ","HJ","C3"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals("highcard",h.getStatus());
        h.getCards().clear();

    }
public void addCards(Hand h, String [] args)
    {
    for(int i = 0; i<args.length;i++)
        {h.addCard(new Card(args[i]));}
    }

}