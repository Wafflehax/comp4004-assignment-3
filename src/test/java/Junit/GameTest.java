package Junit;

import Poker.Game;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import static org.junit.Assert.*;

public class GameTest {

    @Test
    public void test(){
        Scanner read = readIn("InputFile.txt");
        Scanner expected = readIn("ExpectedOutputs.txt");
        read.useDelimiter("\n");
        expected.useDelimiter("\n");
        ArrayList<Game> Tests = new ArrayList<Game>();
        ArrayList<String> ExpectedHand = new ArrayList<String>();

        for(int i = 0; read.hasNext(); i++)
        {Tests.add(new Game(read.next()));}
        read.close();

        for(int i = 0; expected.hasNext(); i++)
        {ExpectedHand.add(new String(expected.next().replace("\r","")));}
        expected.close();

        for(int j = 0; j<Tests.size(); j++)
        {System.out.println("\n----> Poker.Game "+(j+1)+" <-----");
            Tests.get(j).playGame(false,true);
            assertEquals(ExpectedHand.get(j),Tests.get(j).winningHand);
        }
    }

public void checkDeal(Game g){
        //g.deal(g.player, g.aip);
        //assertEquals(52-10,g.gameDeck.getCards().size());
        //assertEquals(5,g.player.hand.getCards().size());
        //assertEquals(5,g.aip.hand.getCards().size());


}

public Scanner readIn(String filename){
    URL path = ClassLoader.getSystemResource(filename);
    File file = null;
    Scanner read = null;
    try {
        file = new File(path.toURI());
    } catch (URISyntaxException e) {
        e.printStackTrace();
    }
    try {
        read = new Scanner(file);
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    }
    return read;
}

}