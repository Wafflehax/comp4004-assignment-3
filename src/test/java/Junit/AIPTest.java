package Junit;

import Poker.AIP;
import Poker.Card;
import Poker.Hand;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class AIPTest {

    @Test
    public void test(){
        AIP aip = new AIP();
        Hand h = aip.hand;

    //isAlmostFlush Tests
        //Is almost straight flush
        addCards(h, new String[]{"DA", "D2","D3","D4","H5"});
        Collections.shuffle(h.getCards());
        assertEquals(true, aip.isAlmostFlush(4));
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

        //Is almost royal flush
        addCards(h, new String[]{"HA", "H10","HJ","HK","D3"});
        Collections.shuffle(h.getCards());
        assertEquals(true, aip.isAlmostFlush(4));
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

        //Is almost flush
        addCards(h, new String[]{"SA", "H2","HJ","HK","H5"});
        Collections.shuffle(h.getCards());
        assertEquals(true, aip.isAlmostFlush(4));
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

        addCards(h, new String[]{"DA", "H10","HJ","HK","D3"});
        Collections.shuffle(h.getCards());
        assertEquals(false, aip.isAlmostFlush(4));
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //isAlmostStraight Tests
        //Testing isAlmostStraight Standard Occurrence
        addCards(h, new String[]{"D2", "S3","H4","H5","D8"});
        Collections.shuffle(h.getCards());
        assertEquals(true, aip.isAlmostStraight());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

        //Testing isAlmostStraight, inside Pair occurrence
        addCards(h, new String[]{"D2", "S3","H4","H5","D3"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.isAlmostStraight());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

        //Testing isAlmostStraight, with a low straight draw (general occurrence)
        addCards(h, new String[]{"D2", "S3","HA","H5","D7"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.isAlmostStraight());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

        //Testing isAlmostStraight, with a low straight draw && inside pair
        addCards(h, new String[]{"D2", "S4","HA","H5","D4"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.isAlmostStraight());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

        //Testing isAlmostStraight, with a low straight draw && inside pair failure
        addCards(h, new String[]{"D2", "S7","HA","H7","D4"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(false, aip.isAlmostStraight());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 3-card flush
        addCards(h, new String[]{"D2", "D7","DA","H7","S4"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.isAlmostFlush(3));
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 3-card flush, failure
        addCards(h, new String[]{"D2", "D7","HA","H7","S4"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(false, aip.isAlmostFlush(3));
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 3-cards of same rank (triple is the lowest rank)
        addCards(h, new String[]{"D2", "H2","HA","S2","S4"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is3OfAKind());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 3-cards of same rank (triple is the middle rank)
        addCards(h, new String[]{"D5", "H5","HA","S5","S4"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is3OfAKind());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 3-cards of same rank (triple is the highest rank)
        addCards(h, new String[]{"DQ", "HQ","HJ","SQ","S4"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is3OfAKind());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing sequence of 3 (sequence is lowest 3-ranks)
        addCards(h, new String[]{"DA", "H2","H3","SQ","S7"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.isSequenceOf3());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing sequence of 3 (sequence is middle 3-ranks)
        addCards(h, new String[]{"S5", "H6","H7","S2","DQ"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.isSequenceOf3());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing sequence of 3 (sequence is highest 3-ranks)
        addCards(h, new String[]{"D2", "HK","HJ","SQ","S4"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.isSequenceOf3());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 2Pairs (To swap is lowest rank)
        addCards(h, new String[]{"D6", "H6","SJ","DJ","C2"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is2Pair());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 2Pairs (To swap is middle rank)
        addCards(h, new String[]{"D2", "H2","SJ","DJ","C6"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is2Pair());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 2Pairs (To swap is highest rank)
        addCards(h, new String[]{"D2", "H2","SJ","DJ","CA"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is2Pair());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 1Pair (Pair-rank is lowest)
        addCards(h, new String[]{"D2", "H2","SJ","D7","C6"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is1Pair());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 1Pair (Pair-rank is 2nd lowest)
        addCards(h, new String[]{"D5", "H5","SJ","D7","C2"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is1Pair());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 1Pair (Pair-rank is 2nd highest)
        addCards(h, new String[]{"D9", "H9","SJ","D7","C6"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is1Pair());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing 1Pair (Pair-rank is highest)
        addCards(h, new String[]{"DA", "HA","SJ","D7","C6"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.is1Pair());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    //Testing High Poker.Card
        addCards(h, new String[]{"DA", "HK","S7","D6","C2"});
        Collections.shuffle(h.getCards());
        h.refreshStatus();
        assertEquals(true, aip.isHighCard());
        aip.printCardsToSwap();
        aip.clearCardsToSwap();
        h.getCards().clear();

    }

    public void addCards(Hand h, String [] args)
    {
        for(int i = 0; i<args.length;i++)
        {h.addCard(new Card(args[i]));}
    }

}