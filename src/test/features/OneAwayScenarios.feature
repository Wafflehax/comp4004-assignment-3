Feature: The ability for AIP to recognize different situations and choose the proper cards-to-swap

  Scenario Outline: Validate AIP one-away from hands, then WINS against HTB
    Given AIP is one away from "<winning-hand>"
    And HTB has "losing" hand
    Then AIP swaps and "wins"
    Examples:
      |winning-hand       |
      |royalflush         |
      |straightflush      |
      |flush              |
      |straight           |
      |fullhouse_3ofakind |
      |fullhouse_2pair    |


  Scenario Outline: Validate AIP one-away from hands, then LOSES against HTB
    Given AIP is one away from "<losing-hand>"
    And HTB has "winning" hand
    Then AIP swaps and "loses"
    Examples:
      |losing-hand       |
      |royalflush         |
      |straightflush      |
      |flush              |
      |straight           |
      |fullhouse_3ofakind |
      |fullhouse_2pair    |

  Scenario Outline: Validate AIP one-away from hands, then LOSES against HTB
    Given AIP is one away from "<losing-hand>"
    And HTB has "winning" hand
    Then AIP swaps and "loses"
    Examples:
      |losing-hand       |
      |royalflush         |
      |straightflush      |
      |flush              |
      |straight           |
      |fullhouse_3ofakind |
      |fullhouse_2pair    |

  Scenario: AIP has 3 cards in sequence and needs to swap the other 2
    Given AIP has three cards in sequence
    Then AIP swaps the out-of-sequence cards

  Scenario: AIP has 3 suited cards and needs to swap the 2 unsuited cards
    Given AIP has three cards to a flush
    Then AIP swaps the unsuited cards

  Scenario: AIP has 1pair, needs to swap the 3 unpaired cards
    Given AIP has one pair
    Then AIP swaps the unpaired cards

  Scenario: AIP has highcard, needs to keep 2 highest rank and swap the other 3
    Given AIP has highcard
    Then AIP swaps the lowest rank cards