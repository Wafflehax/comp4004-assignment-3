Feature: Resolution of who wins the hand between AIP and HTB

  Scenario Outline: Validate HTB royalflush wins over AIP
    Given HTB has "royalflush" and AIP has "<lesserhand>"
    Then HTB wins
    Examples:
      |lesserhand    |
      |straightflush |
      |4ofakind      |
      |fullhouse     |
      |flush         |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate HTB straightflush wins over AIP
    Given HTB has "straightflush" and AIP has "<lesserhand>"
    Then HTB wins
    Examples:
      |lesserhand    |
      |4ofakind      |
      |fullhouse     |
      |flush         |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate HTB 4ofakind wins over AIP
    Given HTB has "4ofakind" and AIP has "<lesserhand>"
    Then HTB wins
    Examples:
      |lesserhand    |
      |fullhouse     |
      |flush         |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate HTB fullhouse wins over AIP
    Given HTB has "fullhouse" and AIP has "<lesserhand>"
    Then HTB wins
    Examples:
      |lesserhand    |
      |flush         |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate HTB flush wins over AIP
    Given HTB has "flush" and AIP has "<lesserhand>"
    Then HTB wins
    Examples:
      |lesserhand    |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate HTB straight wins over AIP
    Given HTB has "straight" and AIP has "<lesserhand>"
    Then HTB wins
    Examples:
      |lesserhand    |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate HTB 3ofakind wins over AIP
    Given HTB has "3ofakind" and AIP has "<lesserhand>"
    Then HTB wins
    Examples:
      |lesserhand    |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate HTB 2pair wins over AIP
    Given HTB has "2pair" and AIP has "<lesserhand>"
    Then HTB wins
    Examples:
      |lesserhand    |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate HTB 1pair wins over AIP
    Given HTB has "1pair" and AIP has "<lesserhand>"
    Then HTB wins
    Examples:
      |lesserhand    |
      |highcard      |


    #Now testing game resolution when AIP has a winning hand over HTB
  Scenario Outline: Validate AIP royalflush wins over HTB
    Given AIP has "royalflush" and HTB has "<lesserhand>"
    Then AIP wins
    Examples:
      |lesserhand    |
      |straightflush |
      |4ofakind      |
      |fullhouse     |
      |flush         |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate AIP straightflush wins over HTB
    Given AIP has "straightflush" and HTB has "<lesserhand>"
    Then AIP wins
    Examples:
      |lesserhand    |
      |4ofakind      |
      |fullhouse     |
      |flush         |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate AIP 4ofakind wins over HTB
    Given AIP has "4ofakind" and HTB has "<lesserhand>"
    Then AIP wins
    Examples:
      |lesserhand    |
      |fullhouse     |
      |flush         |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate AIP fullhouse wins over HTB
    Given AIP has "fullhouse" and HTB has "<lesserhand>"
    Then AIP wins
    Examples:
      |lesserhand    |
      |flush         |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate AIP flush wins over HTB
    Given AIP has "flush" and HTB has "<lesserhand>"
    Then AIP wins
    Examples:
      |lesserhand    |
      |straight      |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |

  Scenario Outline: Validate AIP straight wins over HTB
    Given AIP has "straight" and HTB has "<lesserhand>"
    Then AIP wins
    Examples:
      |lesserhand    |
      |3ofakind      |
      |2pair         |
      |1pair         |
      |highcard      |


  Scenario Outline: Tiebreaking Scenarios
    Given AIP has "<AIPHand>"
    And HTB has "<HTBHand>"
    Then "<Player>" will win
    Examples:
      | AIPHand        | HTBHand        | Player |
      |HA HK HQ HJ H10 |SA SK SQ SJ S10 |HTB     |
      |HK HQ HJ H10 H9 |SQ SJ S10 S9 S8 |AIP     |
      |HQ HJ H10 H9 H8 |SQ SJ S10 S9 S8 |HTB     |
      |HQ SQ DQ CQ H2  |HK SK DK CK H2  |HTB     |
      |HQ SQ DQ H2 S2  |HJ SJ DJ H9 C9  |AIP     |
      |HQ SQ DQ H2 S3  |HJ SJ DJ H9 C8  |AIP     |
      |HQ SJ D10 H9 S8 |CQ DJ S10 C9 H8 |AIP     |
      |HJ CJ D5 H5 S2  |SJ DJ S5 C5 C3  |HTB     |
      |HJ CJ D5 H3 S2  |SJ DJ S5 CK C3  |HTB     |
      |HQ CQ D5 H3 S2  |SJ DJ S5 CK C3  |AIP     |
      |HA CQ D5 H3 S2  |SA DJ S5 CK C3  |HTB     |
      |HA CQ D5 H3 S2  |S4 DJ S5 CK C3  |AIP     |
      |HA H10 H9 H8 H5 |SA S10 S9 S8 S5 |HTB     |
      |HA H10 H9 H8 H5 |SA S10 S9 S8 S4 |AIP     |
      |HA H10 H9 H7 H5 |SA S10 S9 S8 S4 |HTB     |
      |HA H10 H9 H7 H5 |SA S10 S8 S5 S4 |AIP     |
      |HA HQ H10 H7 H5 |SA SK S9 S8 S4  |HTB     |
      |HK HQ H10 H7 H5 |SA SK S9 S8 S4  |HTB     |




