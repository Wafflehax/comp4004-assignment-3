var stompClient = null;
var userList;
var cardsReceived;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

//---------------------------------------------------------------------------------------------
//  START
//  OF
//  LOGIN
//  FUNCTIONS
//---------------------------------------------------------------------------------------------


function connect() {var name = $("#name").val();

    var socket = new SockJS('/gs-guide-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({user:name}, function (frame) {
        setConnected(true);
        stompClient.subscribe('/user/topic/gameAdmin', function (received) {
            adminSetup();
        });

        stompClient.subscribe('/user/topic/fullRoom', function () {
            window.location.href = "fullRoom.html";
        });

        stompClient.subscribe('/topic/userList', function (greeting) {
            userList = JSON.parse(greeting.body);
            populateUserList(userList);
            adminAdjust();
        });

        subscribeToGameUpdates();

        stompClient.subscribe('/topic/gameStart', function () {

            $("#gameBody").load("Poker.html",function(){
                $("#playTable").show();
                $(".card_select").hide();
                $("#swapCardsButton").hide();
                swapButtonListenerConfig();
                stompClient.send("/app/requestHand",{},null);
                stompClient.send("/app/requestStatus",{},null);
                stompClient.send("/app/requestAIUpdates",{},null);
            });

            $("#gameBody").css({'background-color': 'rgba(65,72,65,0.93)'});
        });

        stompClient.send("/app/register",{},null);
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() {

        if($("#name").val() === "")
            {
             $("#name_error").text("Please input a valid name!");
            }
        else
            {
             $("#name").hide();
             $("#name_label").html("<br>  waiting for game to start...").css({'color':'#7e7e7e'});
             $("#name_error").hide();
             connect();
            }
    });
    $( "#disconnect" ).click(function() { disconnect(); });
    $("#adminStartButton").click(function() {adminStartGame();})

    //GAME RIGGING
    var fileInput = $('#rigfile');
    var uploadButton = $('#upload');

    uploadButton.on('click', function() {
        if (!window.FileReader) {
            alert('Your browser is not supported');
            return false;
        }
        var input = fileInput.get(0);

        // Create a reader object
        var reader = new FileReader();
        if (input.files.length) {
            var textFile = input.files[0];
            // Read the file
            reader.readAsText(textFile);
            // When it's loaded, process it
            $(reader).on('load', processFile);
        } else {
            alert('Please upload a file before continuing')
        }
    });

    function processFile(e) {
        var file = e.target.result,
            results;
        console.log("processing file");
        console.log(file);
        var numAI = $("#adminSelect option:selected").val();
        stompClient.send("/app/riggedGame",{},numAI+":"+file);
    }

});

function adminSetup(){
    $("#adminDiv").show();
}

function adminAdjust(){
    var htmlInject = "<option value=\"0\">0</option>";
    var AILimit = 3 - userList.length;
    var j = 0;

    if(userList.length > 1) $("#adminSelect").html(htmlInject);

    while(j<AILimit){
        var jstr = ""+(j+1);
        htmlInject="<option value="+jstr+">"+(j+1)+"</option>";
        $("#adminSelect").append(htmlInject);
        j++;
    }
}

function adminStartGame(){
    stompClient.send("/app/gameInitialize",{},JSON.stringify({'numAI':$("#adminSelect option:selected").val()}));
}

function populateUserList(userList){
    var connectedInject = "";
    var i = 0;

    while(i<userList.length){
        connectedInject+=userList[i]+"<br>";
        i++;
    }

    $("#connected_users").html(connectedInject);
}

//---------------------------------------------------------------------------------------------
//  END
//  OF
//  LOGIN
//  FUNCTIONS
//---------------------------------------------------------------------------------------------

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

//---------------------------------------------------------------------------------------------
//  START
//  OF
//  GAME
//  FUNCTIONS
//---------------------------------------------------------------------------------------------

function subscribeToGameUpdates(){
    stompClient.subscribe('/user/topic/getHand', function (message) {
        cardsReceived = (message.body).split(" ");
        assignCards(cardsReceived);
    });

    stompClient.subscribe('/user/topic/getStatus', function (message) {
        if((message.body).localeCompare("ACTIVE")==0){
            activePlayer();
        }
        else{
            waitingPlayer();
        }
    });

    stompClient.subscribe('/user/topic/gameUpdates', function (message) {
        $("#updateMessages").append("<br>"+message.body);
    });

    stompClient.subscribe('/user/topic/gameOver', function () {
        disconnect();
    });
}

function assignCards(cardMsg){
    $("#c1_select").val(cardMsg[0]);
    $("#c1").text(cardMsg[0]);
    $("#c2_select").val(cardMsg[1]);
    $("#c2").text(cardMsg[1])
    $("#c3_select").val(cardMsg[2]);
    $("#c3").text(cardMsg[2])
    $("#c4_select").val(cardMsg[3]);
    $("#c4").text(cardMsg[3])
    $("#c5_select").val(cardMsg[4]);
    $("#c5").text(cardMsg[4])
}

function activePlayer(){
    $(".card_select").show();
    $("#swapCardsButton").show();
}

function waitingPlayer(){
    $(".card_select").hide();
    $("#swapCardsButton").hide();
}

function swapButtonListenerConfig(){
    $("#swapCardsButton").click(function() {
        //TODO: Send the selected values to the server, and then resolve statuses server-side
        var selectedCards = [];
        var payload = "";
        $('#getSwapSelected input:checked').each(function() {
            selectedCards.push($(this).attr('value'));
        });
        if(selectedCards.length == 0) payload = "none";
        else{
            for(var i = 0; i<selectedCards.length;i++){
             payload+=selectedCards[i]+" ";
            }
            payload.slice(0,-1);
        }


        stompClient.send("/app/endTurn",{},payload);
    })

}

