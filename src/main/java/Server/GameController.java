package Server;

import Poker.AIP;
import Poker.Deck;
import Poker.Game;
import Poker.Player;
import Server.ClientMessage;
import Server.SessionManager;
import com.sun.deploy.util.SessionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.socket.messaging.AbstractSubProtocolEvent;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.ArrayList;

@Controller
public class GameController  implements ApplicationListener<AbstractSubProtocolEvent>  {

    private Game gameModel;
    private int turnCounter;
    private ArrayList<String> aiMsgQueue = new ArrayList<>();

    @Autowired
    public SimpMessageSendingOperations messagingTemplate;

    @Autowired
    public SessionManager sessionManager;

    @Autowired
    public GameStatus gameStatus;

    /*@MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public void greeting() throws Exception {
        Thread.sleep(1000); // simulated delay
        //return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
        System.out.println("sending adminIndex");
        makeAdmin(adminId);
    }*/
    @MessageMapping("/riggedGame")
    @SendTo("/topic/gameStart")
    public String rigGame(@Payload String message){
        int numAI = Integer.parseInt(message.substring(0,1));
        String cards = message.substring(2);
        gameModel = new Game();

        turnCounter = 0;

        for(int i = 0; i<sessionManager.size();i++){gameModel.addServerPlayer(sessionManager.getSessionByIndex(i).getID(),false);}
        for(int i = 0; i<numAI;i++){gameModel.addServerPlayer("AI"+i,true);
            sessionManager.addPlayer("AI"+i,"AI"+i);
        }

        gameModel.rigDeck(cards);
        gameModel.serverGameSetup(true);

        while(gameModel.serverPlayers.get(turnCounter).isAI()){
            aiMsgQueue.add(resolveAI());
        }

        sessionManager.getSessionByID(gameModel.serverPlayers.get(turnCounter).getName()).setState("ACTIVE");

        gameStatus.setGameInProgress(true);


        return "";
    }

    @MessageMapping("/gameInitialize")
    @SendTo("/topic/gameStart")
    public String initPokerGame(ClientMessage message){
        gameModel = new Game();
        turnCounter = 0;

        for(int i = 0; i<sessionManager.size();i++){gameModel.addServerPlayer(sessionManager.getSessionByIndex(i).getID(),false);}
        for(int i = 0; i<message.getNumAI();i++){gameModel.addServerPlayer("AI"+i,true);
        sessionManager.addPlayer("AI"+i,"AI"+i);
        }

        gameModel.serverGameSetup(false);

        while(gameModel.serverPlayers.get(turnCounter).isAI()){
            aiMsgQueue.add(resolveAI());
        }

        sessionManager.getSessionByID(gameModel.serverPlayers.get(turnCounter).getName()).setState("ACTIVE");

        gameStatus.setGameInProgress(true);


        return "";
        //System.out.println(sessionManager.toString());
    }

    @MessageMapping("/requestHand")
    @SendTo("/topic/nobody")
    public String cardsToClient(){
        for(int i = 0; i<gameModel.serverPlayers.size(); i++){
            AIP check = gameModel.serverPlayers.get(i);
            if(!check.isAI()){
                sendHand(check.getName(),check.hand.toString());
            }
        }
        return "";
        //System.out.println(sessionManager.toString());
    }

    @MessageMapping("/requestStatus")
    @SendTo("/topic/nobody")
    public String statusToClient(){

        Session sendTo;
        for(int i = 0; i<sessionManager.size(); i++){
            sendTo = sessionManager.getSessionByIndex(i);

            if (sendTo.getID().substring(0,2).compareTo("AI")!=0) sendStatus(sendTo.getID(),sendTo.getState());
        }
        return "";
        //System.out.println(sessionManager.toString());
    }

    @MessageMapping("/requestAIUpdates")
    @SendTo("/topic/nobody")
    public String aiUpdates(){
        for(int i = 0; i<aiMsgQueue.size();i++){
            updateAll(aiMsgQueue.get(i));
        }

        return "";
    }



    @MessageMapping("/endTurn")
    @SendTo("/topic/nobody")
    public String handleEndTurn(@Header("simpSessionId") String sessionId, @Payload String message){
        Session client = sessionManager.getSessionByID(sessionId);
        ArrayList<String> cardsToSwap = new ArrayList<>();
        String update;


        String cardsToSwapArray[] = message.split(" ");

        if(cardsToSwapArray[0].compareTo("none")==0) update = client.getUsername()+" has swapped 0 cards.";
        else update = client.getUsername()+" has swapped "+cardsToSwapArray.length+" cards.";

        for(int i = 0; i<cardsToSwapArray.length;i++){
            cardsToSwap.add(cardsToSwapArray[i]);
        }

        gameModel.serverSetCardsToSwap(sessionId,cardsToSwap);
        gameModel.serverSwapCards(sessionId);

        client.setState("WAITING");
        sendHand(sessionId,gameModel.getSession(sessionId).hand.toString());

        turnCounter = (turnCounter + 1)%sessionManager.size();
        updateWaiting(update);

        //CHECK ENDGAME
        while(gameModel.serverPlayers.get(turnCounter).isAI() && turnCounter!=0){
            resolveAI();
        }

        if (turnCounter==0){
            System.out.println("GAME FINISHED");
            setPlayerStatuses("WAITING");
            resolveGameWinner();
            return "";
        }
        else{
            setPlayerStatuses("ACTIVE");
        }





        return "";
    }

    /*@MessageMapping("/gameInput")
    @SendTo("/topic/gameUpdates")
    public String gameInputAndUpdate(ClientMessage message){
        System.out.println("received input!");
        System.out.println(message.getNumAI());
        return "placeholder";
        //System.out.println(sessionManager.toString());
    }*/

    @Override
    public void onApplicationEvent(AbstractSubProtocolEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        MessageHeaders headers = event.getMessage().getHeaders();

        if (event instanceof SessionDisconnectEvent) {
            SessionDisconnectEvent disconnectEvent = (SessionDisconnectEvent) event;

            String id = disconnectEvent.getSessionId();

            if(gameModel != null) gameModel.droppedServerPlayer(id);

            boolean anyRemaining = false;
            for(int i = 0; i<sessionManager.size();i++){
                if(sessionManager.getSessionByIndex(i).getID().substring(0,2).compareTo("AI")!=0) anyRemaining=true;
            }
            if(!anyRemaining){
                sessionManager.clear();
                gameStatus.setGameInProgress(false);
                gameModel = null;
                turnCounter = 0;
            }

        }
    }

    private void sendHand(String sessionId, String hand){
        sendToSession(sessionId,"/topic/getHand",hand);
    }

    private void sendStatus(String sessionId, String status){
        sendToSession(sessionId,"/topic/getStatus",status);
    }

    private void sendToSession(String sessionId, String subscription,String message) {

        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        headerAccessor.getMessageHeaders();

        messagingTemplate.convertAndSendToUser(sessionId,subscription, message,
                headerAccessor.getMessageHeaders());


    }

    private void refreshClientUserList(){
        messagingTemplate.convertAndSend("/topic/userList", sessionManager.getPlayerList());
    }

    private String resolveAI(){
        AIP ai = gameModel.serverPlayers.get(turnCounter);
        ai.runAI();

        String update = ai.getName()+" has swapped "+ai.getCardsToSwap().size()+" cards.";
        updateWaiting(update);

        gameModel.serverSwapCards(ai.getName());
        gameModel.serverReplenishHand(ai);
        turnCounter = (turnCounter + 1)%sessionManager.size();
        return update;
    }

    private void setPlayerStatuses(String determinant){
        int size = sessionManager.size();
        for(int i = 0; i<size;i++){
            AIP check = gameModel.serverPlayers.get(i);

            if(check.isAI()){
                //do nothing
            }
            else if(i == turnCounter){
                sessionManager.getSessionByID(check.getName()).setState(determinant);
            }
            else{
                sessionManager.getSessionByID(check.getName()).setState("WAITING");
            }
        }
        statusToClient();
    }


    private void updateWaiting(String message) {
        int size = sessionManager.size();
        for(int i = 0; i<size;i++){
            Session toMessage = sessionManager.getSessionByIndex(i);

            if(toMessage.getID().substring(0,2).compareTo("AI")!=0 && toMessage.getState().compareTo("WAITING")==0){
                sendToSession(toMessage.getID(),"/topic/gameUpdates",message);
            }

        }
    }

    private void updateAll(String message) {
        int size = sessionManager.size();
        for(int i = 0; i<size;i++){
            Session toMessage = sessionManager.getSessionByIndex(i);

            if(toMessage.getID().substring(0,2).compareTo("AI")!=0){
                sendToSession(toMessage.getID(),"/topic/gameUpdates",message);
            }

        }
    }

    private void resolveGameWinner() {
        turnCounter = 0;
        Player winner = gameModel.serverResolveWinner();
        String update = sessionManager.getSessionByID(winner.getName()).getUsername()+" wins!";
        updateAll(update);

        for(int i = 0; i<sessionManager.size();i++){
            AIP temp = gameModel.serverPlayers.get(i);
            update = sessionManager.getSessionByID(temp.getName()).getUsername() + " had " + temp.hand.toString();
            updateAll(update);
        }

        sessionManager.removeAISessions();

        for(int i = 0; i<sessionManager.size();i++)
            sendToSession(sessionManager.getSessionByIndex(i).getID(),"/topic/gameOver","endGame");

        gameStatus.setGameInProgress(false);
        gameModel = null;

    }


}

