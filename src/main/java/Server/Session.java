package Server;

public class Session {
    private boolean admin = false;
    private String state = "WAITING";
    private String ID;
    private String username;


    public Session(String id, String name){
        ID = id;
        username = name;
    }

    public boolean isAdmin(){return admin;}
    public void setAdmin(boolean set){admin = set;}

    public String getState(){return state;}
    public void setState(String set){state = set;}

    public String getID(){return ID;}
    public void setID(String id){ID = id;}

    public String getUsername(){return username;}
    public void setUsername(String set){username = set;}

    @Override
    public String toString(){
        return " -ID:"+ID+", username:"+username+", isAdmin:"+admin+", State:"+state+"- ";
    }
}
