package Server;

import Server.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.AbstractSubProtocolEvent;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.ArrayList;

@Controller
public class LoginController implements ApplicationListener<AbstractSubProtocolEvent> {


    private String adminId = "";
    private String connectingId = "";
    private boolean hasAdmin = false;

    @Autowired
    public SimpMessageSendingOperations messagingTemplate;

    @Autowired
    public SessionManager sessionManager;

    @Autowired
    public GameStatus gameStatus;

    /*@MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public void greeting() throws Exception {
        Thread.sleep(1000); // simulated delay
        //return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
        System.out.println("sending adminIndex");
        makeAdmin(adminId);
    }*/

    @MessageMapping("/register")
    @SendTo("/topic/userList")
    public ArrayList<String> register(){
        ArrayList<String> playerList = sessionManager.getPlayerList();
        System.out.println(sessionManager.toString());
        System.out.println(connectingId);


        if(!sessionManager.contains(connectingId) || gameStatus.isInProgress()) sendToSession(connectingId,"/topic/fullRoom");
        else if(!hasAdmin && adminId.compareTo("") != 0) sendToSession(adminId,"/topic/gameAdmin");

        connectingId = "";
        return playerList;
    }

    @Override
    public void onApplicationEvent(AbstractSubProtocolEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        MessageHeaders headers = event.getMessage().getHeaders();

        if (event instanceof SessionConnectEvent) {
            String id = SimpMessageHeaderAccessor.getSessionId(headers);
            connectingId = id;
            String playerName = sha.getFirstNativeHeader("user");

            if(sessionManager.size()<3)
            {
                sessionManager.addPlayer(id,playerName);
                if(sessionManager.size() == 1) {adminId = id;}
            }



        } else if (event instanceof SessionDisconnectEvent) {
            SessionDisconnectEvent disconnectEvent = (SessionDisconnectEvent) event;

            String id = disconnectEvent.getSessionId();
            System.out.println("Disconnected id: "+id);
            sessionManager.removePlayerBySession(id);
            
            refreshClientUserList();

            if(id.compareTo(adminId)==0){
                hasAdmin = false;
                assignNewAdmin();
            }

        }
    }

    private void assignNewAdmin() {
        adminId = "";
        ArrayList<String> playerList = sessionManager.getPlayerList();

        if(playerList.size()>0)
        {
            adminId = sessionManager.getSessionByIndex(0).getID();
            sendToSession(adminId,"/topic/gameAdmin");
        }

    }

    private void sendToSession(String sessionId, String subscription) {
        adminId = sessionId;
        hasAdmin = true;
        SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.create(SimpMessageType.MESSAGE);
        headerAccessor.setSessionId(sessionId);
        headerAccessor.setLeaveMutable(true);
        headerAccessor.getMessageHeaders();

        messagingTemplate.convertAndSendToUser(sessionId,subscription, "",
                headerAccessor.getMessageHeaders());


    }

    private void refreshClientUserList(){
        messagingTemplate.convertAndSend("/topic/userList", sessionManager.getPlayerList());
    }

}

