package Server;

import org.springframework.stereotype.Component;

@Component
public class GameStatus {
    private boolean gameInProgress = false;

    public boolean isInProgress(){return gameInProgress;}
    public void setGameInProgress(boolean set){gameInProgress = set;}
}
