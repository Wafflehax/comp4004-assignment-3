package Server;

public class ClientMessage {

    private String numAI;

    public ClientMessage() {
    }

    public ClientMessage(String numAIPlayers) {
        this.numAI = numAIPlayers;
    }

    public int getNumAI() {
        return Integer.parseInt(numAI);
    }

    public void setNumAI(int numAI) {
        this.numAI = ""+numAI;
    }
}
