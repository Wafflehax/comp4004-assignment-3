package Server;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

@Component
public class SessionManager {
    private HashMap<String,String> SessionsPlayerMap = new HashMap<String,String>();
    private ArrayList<Session> sessionsList = new ArrayList<>();

    private boolean isUniqueName(String name){

        for(int i = 0; i<sessionsList.size();i++)
        {
         if(name.compareTo(sessionsList.get(i).getUsername())==0) return false;
        }

        return true;
    }

    public void addPlayer(String sessionID,String user){
        if(!isUniqueName(user)){
            addPlayerUnique(sessionID,user);
        }
        else sessionsList.add(new Session(sessionID,user));
    }

    public void addPlayerUnique(String sessionID,String user){
        int append = 1;
        while(!isUniqueName(user+append)){
            append++;
        }
        String newUser = user + append;
        addPlayer(sessionID, newUser);
    }

    public Session removePlayerBySession(String sessionID){
        for(int i = 0; i<sessionsList.size(); i++)
        {Session check = sessionsList.get(i);
            if(check.getID().compareTo(sessionID)==0) {sessionsList.remove(check); return check;}
        }
        return null;
    }

    public void removeAISessions(){
        for(int i = 0; i<sessionsList.size(); i++)
        {Session check = sessionsList.get(i);
            if(check.getID().substring(0,2).compareTo("AI")==0) sessionsList.remove(check);
        }
    }

    public String toString(){
        return sessionsList.toString();
    }

    public int size(){return sessionsList.size();}

    public ArrayList<String> getPlayerList(){
        ArrayList<String> playerList = new ArrayList<>();

        for(int i = 0; i<sessionsList.size(); i++){
            playerList.add(sessionsList.get(i).getUsername());
        }

        return playerList;
    }

    public boolean contains(String sessionID){
        for(int i = 0; i<sessionsList.size(); i++){
            Session check = sessionsList.get(i);
            if(check.getID().compareTo(sessionID)==0)return true;
        }
        return false;
    }

    public Session getSessionByID(String ID) {
        Session check;

        for(int i = 0; i<sessionsList.size();i++){
            check = sessionsList.get(i);
            if(check.getID().compareTo(ID)==0) return check;
        }

        return null;
    }

    public Session getSessionByIndex(int index) {
        return sessionsList.get(index);
    }

    public Session getAdmin(){
        for(int i = 0; i<sessionsList.size();i++){
            Session check = sessionsList.get(i);
            if(check.isAdmin()) return check;
        }

        return null;
    }

    public void clear(){
        sessionsList.clear();
    }

    public void convertSessionToAI(String id){
        getSessionByID(id).setState("WAITING");
        getSessionByID(id).setID("AI_"+getSessionByID(id).getID());
    }
}
