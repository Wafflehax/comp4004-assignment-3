package Poker;

public class Player {
    public Hand hand;
    protected String name = "HTB";
    protected boolean isAI;

    public Player() {
        this.hand = new Hand();
    }


    public String getName() {
        return name;
    }
    public void setName(String user){this.name = user;}
    public void setIsAI(boolean ai){this.isAI = ai;}
    public boolean isAI(){return this.isAI;}



    protected void showHand(){
        System.out.println(name+"'s hand: ");
        for(int i = 0; i<hand.getCards().size();i++)
        {System.out.print(hand.getCards().get(i));}
        System.out.println();
}
}
