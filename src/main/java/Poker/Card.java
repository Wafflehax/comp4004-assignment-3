package Poker;

public class Card implements Comparable<Card>{
    private String suit;
    private int rank;
    private String rankS;
    private int suitScore;
    public Card(String name){
        this.suit = name.substring(0,1);
        this.rank = rankConvert(name.substring(1,2));
        this.rankS = name.substring(1,2);
        if(this.rankS.compareTo("1")==0) rankS = "10";
        this.suitScore = suitConvert(this.suit);
    }

public String getSuit() {return this.suit;}
public int getRank() {return this.rank;}
public String getRankS(){return rankS;}
public int getSuitScore(){return suitScore;}
public void printCard() {System.out.print(this.suit + this.rankS + " ");}

public int rankConvert(String r){
        if(r.compareTo("2")==0) return 2;
        else if(r.compareTo("3")==0) return 3;
        else if(r.compareTo("4")==0) return 4;
        else if(r.compareTo("5")==0) return 5;
        else if(r.compareTo("6")==0) return 6;
        else if(r.compareTo("7")==0) return 7;
        else if(r.compareTo("8")==0) return 8;
        else if(r.compareTo("9")==0) return 9;
        else if(r.compareTo("1")==0) return 10;
        else if(r.compareTo("J")==0) return 11;
        else if(r.compareTo("Q")==0) return 12;
        else if(r.compareTo("K")==0) return 13;
        else if(r.compareTo("A")==0) return 14;


    return -1;
}

public int suitConvert(String suit){
if(suit.compareTo("S")==0) return 4;
else if (suit.compareTo("H")==0) return 3;
else if (suit.compareTo("D")==0) return 2;
else if (suit.compareTo("C")==0) return 1;
else return 0;
}


    public int compareTo(Card compareCard) {
        int compareRank=((Card)compareCard).getRank();
        /* For Ascending order*/
        return this.rank-compareRank;

    }

public Card higherCard (Card c2){
if(this.getRank()>c2.getRank()) return this;
else if (this.getRank() < c2.getRank()) return c2;
else
    {
        if(this.suitScore < c2.suitScore) return c2;
        else return this;

    }
    }

    @Override
    public String toString() {
        return suit+rankS;
    }
}
