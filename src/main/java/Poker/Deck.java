package Poker;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Deck {
    private ArrayList<Card> Cards;


    public Deck() {
        Cards = new ArrayList<Card>();
        Scanner read = readIn();

        for(int i = 0; i<52 && read.hasNext(); i++)
        {Cards.add(new Card(read.next()));}

        read.close();
    }
    public Deck(String input) {
        Cards = new ArrayList<Card>();
        String[] card = input.split(" ");

        for(int i = 0; i<card.length; i++)
        {Cards.add(new Card(card[i]));}
    }

    private Scanner readIn(){URL path = ClassLoader.getSystemResource("TextFiles/TextDeck.txt");
        File file = null;
        Scanner read = null;
        try {
            file = new File(path.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        try {
            read = new Scanner(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return read;
    }

    public Card getCardAt(int i){return Cards.get(i);}
    public ArrayList<Card> getCards(){return Cards;}

    public Card takeTopCard(){return Cards.remove(0);}
    public void returnCardToDeck(ArrayList<Card> returning){Cards.addAll(returning);}

    public void shuffleDeck(){
        Collections.shuffle(Cards);
    }

    public void printDeck(){
        for(int i = 0; i<Cards.size();i++)
            Cards.get(i).printCard();
    }
}
