package Poker;

import java.util.ArrayList;
import java.util.Collections;

public class AIP extends Player {

    private ArrayList<Card> CardsToSwap;

    public AIP() {super();
    this.name = "AIPlayer";
    this.CardsToSwap = new ArrayList<Card>();
    this.isAI = true;
    }

    public AIP(boolean ai, String user) {super();
        this.name = user;
        this.CardsToSwap = new ArrayList<Card>();
        this.isAI = ai;
    }



public void runAI(){
        clearCardsToSwap();
        hand.refreshStatus();
        if(hand.getStatusScore()>3) return;
        else if(isAlmostFlush(4))return;
        else if(isAlmostStraight()) return;
        else if (is3OfAKind()) return;
        else if(isAlmostFlush(3)) return;
        else if (isSequenceOf3()) return;
        else if (is2Pair()) return;
        else if(is1Pair()) return;
        else isHighCard();
}

public boolean isAlmostFlush(int suitsToFlush){
        boolean isAlmostFlush = false;
        int [] suitCount = new int[4];
        int suitToKeep = 0;

        for(int i = 0; i<hand.getCards().size(); i++)
            {int suit = hand.getCards().get(i).getSuitScore();
            suitCount[suit - 1]++;
            }
        isAlmostFlush = (suitCount[0] == suitsToFlush || suitCount [1] == suitsToFlush || suitCount[2] == suitsToFlush || suitCount[3] == suitsToFlush);


        if(isAlmostFlush)
        {for(int i = 0; i<4; i++)
            {if(suitCount[i]==suitsToFlush) suitToKeep = i+1;
            }
         for(int i = 0; i<hand.getCards().size(); i++)
            {if(hand.getCards().get(i).getSuitScore() != suitToKeep) CardsToSwap.add(hand.getCards().get(i));
            }
        }

        return isAlmostFlush;
}

public boolean isAlmostStraight(){
int diff;
int straightScore;
Card prev;
Hand check = new Hand();

check.getCards().addAll(hand.getCards());
Collections.sort(check.getCards());
Collections.reverse(check.getCards());

diff = check.getCards().get(0).getRank()-check.getCards().get(4).getRank();

if(hand.getStatusScore()==1 && (diff == 3 || diff == 4)) {pairResolution(check); return true;};
if(check.getCards().get(0).getRank()==14 && (diff == 12 || diff == 11)) return lowStraightCheck(check); //for the special case where it's a low straight draw featuring an Ace.

straightScore = 0;
prev = check.getCards().get(0);
for(int i = 1; i<check.getCards().size()-1;i++)
    {diff = prev.getRank() - check.getCards().get(i).getRank();
    prev = check.getCards().get(i);

    if(diff == 1 || diff == 2) straightScore+=diff;
    else {straightScore = 0; i = 5;}


    }
if(straightScore==3 || straightScore == 4){CardsToSwap.add(check.getCards().get(check.getCards().size()-1)); return true;}

straightScore = 0;
prev = check.getCards().get(1);
for (int i = 2; i < check.getCards().size(); i++) {
        diff = prev.getRank() - check.getCards().get(i).getRank();
        prev = check.getCards().get(i);

        if(diff == 1 || diff == 2) straightScore+=diff;
        else {straightScore = 0; i = 5;}


    }

if(straightScore==3 || straightScore == 4){CardsToSwap.add(check.getCards().get(0)); return true;}

return false;
}

public boolean is3OfAKind(){
        int diff;
        int diff2;
        Hand check = new Hand();
        check.getCards().addAll(hand.getCards());
        check.refreshStatus();
        Collections.sort(check.getCards());
        diff = check.getCards().get(1).getRank() - check.getCards().get(0).getRank();
        diff2 = check.getCards().get(2).getRank() - check.getCards().get(1).getRank();

        if(check.getStatusScore()==3){
            if (diff == 0 && diff2 == 0){CardsToSwap.add(check.getCards().get(3)); CardsToSwap.add(check.getCards().get(4));}
            else if(diff != 0 && diff2 == 0){CardsToSwap.add(check.getCards().get(0)); CardsToSwap.add(check.getCards().get(4));}
            else {CardsToSwap.add(check.getCards().get(0)); CardsToSwap.add(check.getCards().get(1));}



        return true;
        }
        return false;
}

public boolean isSequenceOf3(){
        Hand check = new Hand();
        int [] diff = new int[4];
        int [] rank = new int[5];

        check.getCards().addAll(hand.getCards());
        Collections.sort(check.getCards());

        for(int i = 0; i<check.getCards().size();i++)
            {rank[i] = check.getCards().get(i).getRank();}

        for(int i = 0; i<4; i++)
            {diff[i] = rank[i+1] - rank[i];}

        if(rank[4]==14 && rank[0]==2 && rank[1] == 3)
            {
                CardsToSwap.add(check.getCards().get(3));
                CardsToSwap.add(check.getCards().get(2));
                return true;
            }
        else if(diff[0]==1 && diff[1] == 1)
        {
            CardsToSwap.add(check.getCards().get(3));
            CardsToSwap.add(check.getCards().get(4));
            return true;
        }
        else if(diff[1]==1 && diff[2] == 1)
        {
            CardsToSwap.add(check.getCards().get(0));
            CardsToSwap.add(check.getCards().get(4));
            return true;
        }
        else if(diff[2]==1 && diff[3] == 1)
        {
            CardsToSwap.add(check.getCards().get(0));
            CardsToSwap.add(check.getCards().get(1));
            return true;
        }




        return false;
}

public boolean is2Pair(){
    Hand check = new Hand();
    int [] diff = new int[4];
    int [] rank = new int[5];

    check.getCards().addAll(hand.getCards());
    Collections.sort(check.getCards());
    check.refreshStatus();

    if(check.getStatusScore()!=2) return false;

    for(int i = 0; i<check.getCards().size();i++)
    {rank[i] = check.getCards().get(i).getRank();}

    for(int i = 0; i<4; i++)
    {diff[i] = rank[i+1] - rank[i];}

    if(diff[0]==0 && diff[2] == 0 && diff[3] != 0){CardsToSwap.add(check.getCards().get(4)); return true;}
    else if(diff[0]==0 && diff[3] == 0 && diff[1] != 0){CardsToSwap.add(check.getCards().get(2)); return true;}
    else if(diff[1]==0 && diff[3] == 0 && diff[0] != 0){CardsToSwap.add(check.getCards().get(0)); return true;}

        return false;
}

public boolean is1Pair(){
    Hand check = new Hand();
    int [] diff = new int[4];
    int [] rank = new int[5];

    check.getCards().addAll(hand.getCards());
    Collections.sort(check.getCards());
    check.refreshStatus();

    if(check.getStatusScore()!=1) return false;

    for(int i = 0; i<check.getCards().size();i++)
    {rank[i] = check.getCards().get(i).getRank();}

    for(int i = 0; i<4; i++)
    {diff[i] = rank[i+1] - rank[i];}

    if(diff[0]==0)
        {CardsToSwap.add(check.getCards().get(2));
        CardsToSwap.add(check.getCards().get(3));
        CardsToSwap.add(check.getCards().get(4));
        return true;
        }
    else if(diff[1]==0)
        {CardsToSwap.add(check.getCards().get(0));
        CardsToSwap.add(check.getCards().get(3));
        CardsToSwap.add(check.getCards().get(4));
        return true;
        }
    else if(diff[2]==0)
        {CardsToSwap.add(check.getCards().get(0));
        CardsToSwap.add(check.getCards().get(1));
        CardsToSwap.add(check.getCards().get(4));
        return true;
        }
    else
        {CardsToSwap.add(check.getCards().get(0));
        CardsToSwap.add(check.getCards().get(1));
        CardsToSwap.add(check.getCards().get(2));
        return true;
        }
}

public boolean isHighCard(){
    Hand check = new Hand();
    int [] diff = new int[4];
    int [] rank = new int[5];

    check.getCards().addAll(hand.getCards());
    Collections.sort(check.getCards());
    check.refreshStatus();

    if(check.getStatusScore()!=0) return false;
    CardsToSwap.add(check.getCards().get(0));
    CardsToSwap.add(check.getCards().get(1));
    CardsToSwap.add(check.getCards().get(2));

    return true;

}


//AlmostStraight helper methods
public void pairResolution(Hand check) {
    Card prev = check.getCards().get(0);
    for (int i = 1; i < check.getCards().size(); i++) {
        if (prev.getRank() - check.getCards().get(i).getRank()==0)
            {
                CardsToSwap.add(prev);
                i = 5;
            }
        else prev = check.getCards().get(i);
    }
}

public boolean lowStraightCheck(Hand check){
    int diff = check.getCards().get(0).getRank() - check.getCards().get(1).getRank();
    int diff2 = check.getCards().get(0).getRank() - check.getCards().get(2).getRank();
    int straightScore;
    boolean isPair;

    check.refreshStatus();
    isPair = check.getStatusScore()==1;

    if(isPair)
    {
        if(diff == 10 || diff == 9){
            pairResolution(check); return true;
        }

        else if(diff == 0 && (diff2 == 10 || diff2 == 9))
        {
            pairResolution(check); return false;
        }

    }

    else
    {diff = check.getCards().get(0).getRank() - check.getCards().get(4).getRank();

        if(diff == 12) diff = 1;
        else diff = 2;

        straightScore = diff;
        Card prev = check.getCards().get(2);

        for(int i = 3; i<check.getCards().size(); i++)
        {
            diff = prev.getRank() - check.getCards().get(i).getRank();
            prev = check.getCards().get(i);

            if (diff == 1 || diff == 2) straightScore += diff;
            else {
                straightScore = 0;
                i = 5;
            }
        }
        if(straightScore==3 || straightScore == 4){CardsToSwap.add(check.getCards().get(1)); return true;}


    }




return false;
}


public void printCardsToSwap(){
        System.out.println();
        System.out.println("AIP: Cards to Swap");
    for(int i = 0; i<CardsToSwap.size();i++)
    {CardsToSwap.get(i).printCard();}

    if(CardsToSwap.size()==0) System.out.println("No Cards to Swap");
}

public void clearCardsToSwap(){CardsToSwap.clear();}

    public ArrayList<Card> getCardsToSwap() {
        return CardsToSwap;
    }

    public ArrayList<Card> SwapCards(){
        for(int i = 0; i<CardsToSwap.size(); i++)
        {hand.removeCard(CardsToSwap.get(i));}
return CardsToSwap;
}

public void addCardToSwap(String card){
        ArrayList<Card> checkCards = hand.getCards();

        for (int i = 0; i<checkCards.size(); i++){
            Card check = checkCards.get(i);
            if(check.toString().compareTo(card)==0) CardsToSwap.add(check);
        }
}


}
