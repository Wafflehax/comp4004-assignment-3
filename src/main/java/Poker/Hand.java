package Poker;

import java.util.ArrayList;
import java.util.Collections;

public class Hand {
    private ArrayList<Card> Cards;
    private String status;
    private int statusScore;

    public Hand(){Cards = new ArrayList<Card>();
    }
public void addCard(Card c){Cards.add(c);}
public void removeCard(Card c){Cards.remove(c);}
public String getStatus(){return status;}
public int getStatusScore(){return statusScore;}
public ArrayList<Card> getCards(){return Cards;}

    public void refreshStatus(){
if(isRoyalFlush()) {status = "royalflush"; statusScore = 9;}
else if (isStraightFlush()) {status = "straightflush"; statusScore = 8;}
else if (isFourOfAKind()) {status = "4ofakind"; statusScore = 7;}
else if (isFullHouse()) {status = "fullhouse"; statusScore = 6;}
else if (isFlush()) {status = "flush"; statusScore = 5;}
else if (isStraight()) {status = "straight"; statusScore = 4;}
else if (isThreeOfAKind()) {status = "3ofakind"; statusScore = 3;}
else if (isTwoPair()) {status = "2pair"; statusScore = 2;}
else if (isPair()) {status = "1pair"; statusScore = 1;}
else {status = "highcard"; statusScore = 0;}
}

public boolean isRoyalFlush(){
    return (isFlush() && isStraight() && (Collections.min(Cards).getRank() == 10));
    }

public boolean isStraightFlush(){return (isStraight() && isFlush() && (Collections.max(Cards).getRank() < 14));}

public boolean isFourOfAKind(){
        Card check = Cards.get(0);
        int count = 1;

        for(int i = 1; i<Cards.size();i++)
        {if(check.getRank()==Cards.get(i).getRank()) count++;
        }

        if(count<4)
        {check = Cards.get(1);
         count = 1;

        for(int i = 2; i<Cards.size();i++)
            {if(check.getRank()==Cards.get(i).getRank()) count++;
            }
        }



        return count == 4;}

public boolean isFullHouse(){
        return !isFourOfAKind() && distinctCards() == 2;}

public boolean isFlush(){
        boolean isFlush = true;
        Card check = Cards.get(0);
    for(int i = 1; (i<Cards.size() && isFlush); i++)
        {isFlush = (check.getSuit().compareTo(Cards.get(i).getSuit()) == 0);
            check = Cards.get(i);
        }
return isFlush;
}

public boolean isStraight(){
        boolean isStraight = true;
        ArrayList<Card> temp = new ArrayList<Card>();
        temp.addAll(Cards);
        Collections.sort(temp);
        Card check = temp.get(0);


        for(int i = 1; (i<temp.size() && isStraight);i++)
            {
                isStraight = (temp.get(i).getRank() - check.getRank() == 1) || ((temp.get(i).getRank() - check.getRank() == 9) && temp.get(i).getRank() == 14);
                check = temp.get(i);
            }



        return isStraight;}

public boolean isThreeOfAKind(){
        int numDistinct = distinctCards();
        if(numDistinct != 3) return false;

    Card check = Cards.get(0);
    int count = 1;

    for(int i = 1; i<Cards.size();i++)
    {if(check.getRank()==Cards.get(i).getRank()) count++;
    }

    if(count<3)
    {check = Cards.get(1);
        count = 1;

        for(int i = 2; i<Cards.size();i++)
        {if(check.getRank()==Cards.get(i).getRank()) count++;
        }
    }

    if(count<3)
    {check = Cards.get(2);
        count = 1;

        for(int i = 3; i<Cards.size();i++)
        {if(check.getRank()==Cards.get(i).getRank()) count++;
        }
    }



    return count == 3;
    }

public boolean isTwoPair(){return distinctCards() == 3;}

public boolean isPair(){return distinctCards() == 4;}

public int distinctCards(){
        int numOfDistinct = 0;

        ArrayList<Integer> distinctCards = new ArrayList<Integer>();

        for(int i=0; i<Cards.size(); i++){
            if(!distinctCards.contains(Cards.get(i).getRank())){
                distinctCards.add(Cards.get(i).getRank());
            }
        }

        if(distinctCards.size()==1){
            numOfDistinct = 0;
        }
        else{
            numOfDistinct = distinctCards.size();
        }

        return numOfDistinct;
    }

public void printHand(){
        for(int i = 0; i<Cards.size();i++)
        {Cards.get(i).printCard();}
        System.out.println();
}

public String toString(){
        String out = "";

        for(int i=0; i<Cards.size();i++)
        {out = out + Cards.get(i).toString()+" ";}
return out;
}


}
